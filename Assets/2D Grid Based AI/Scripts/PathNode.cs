﻿using UnityEngine;
using System.Collections;
using System;

public class PathNode : SettlersEngine.IPathNode<object>
{
	public Int32 X { get; set; }
	public Int32 Y { get; set; }
	public Boolean IsWall {get; set;}
	
	public bool IsWalkable(object unused)
	{
		return !IsWall;
	}
}

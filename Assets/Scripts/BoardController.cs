﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BoardController : MonoBehaviour {
    public int columns;
    public int rows;
    public GameObject[] floors;
    public GameObject[] outerWalls;
    private Transform gameBoard;
    public GameObject[] wallObstacles;
    public PathNode[,] grid;
    public GameObject finish;

    private void SetupGameBoard()
    {
        grid = new PathNode[columns, rows];
        gameBoard = new GameObject("Game Board").transform;
        for (int x = 1; x < columns - 1; x++)
        {
            for (int y = 1; y < rows - 1; y++)
            {
                GameObject selectedTile;
                selectedTile = floors[Random.Range(0, floors.Length)];
                grid[x, y] = new PathNode()
                {
                    X = x,
                    Y = y,
                    IsWall = false
                };
                GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector2(x, y), Quaternion.identity);
                floorTile.transform.SetParent(gameBoard);
            }
        }
        int width = columns,
            height = rows;
        MazeCreator mazeGenerator = new MazeCreator(width, height, new System.Drawing.Point(1, height - 2));
        mazeGenerator.FurthestPoint = new System.Drawing.Point(width - 2, height - 2);
        byte[,] maze = mazeGenerator.CreateMaze();
        maze[width - 2, height - 2] = 0; // make sure furthest point is empty
        var wallTile = outerWalls[Random.Range(0, outerWalls.Length)];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (maze[x, y] == 0)
                    continue;
                GameObject wall = (GameObject)Instantiate(wallTile, new Vector2(x, y), Quaternion.identity);
                wall.transform.SetParent(gameBoard);
                if (grid[x, y] == null)
                {
                    grid[x, y] = new PathNode()
                    {
                        IsWall = true,
                        X = x,
                        Y = y
                    };
                }
                grid[x, y].IsWall = true;
            }
        }
        // create final point
        GameObject finishGameObject = (GameObject)Instantiate(finish, new Vector2(width - 2, height - 2), Quaternion.identity);
        finishGameObject.transform.SetParent(gameBoard);
    }
    public void SetupLevel(int currentLevel)
    {
        rows = columns = columns + currentLevel * 5;
        SetupGameBoard();
    }
}

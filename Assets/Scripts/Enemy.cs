﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

public class Enemy : MovingObject {
    private GameController gameController;
    private BoardController boardController;
    IEnumerable<PathNode> path;
    PathNode[,] grid;
    float waitTime = 0.5f;
    public void Awake()
    {
        Visible = false;
    }
    public void Start()
    {
        gameController = GameObject.Find("GameController").GetComponent<GameController>();
        boardController = gameController.GetComponent<BoardController>();
        grid = boardController.grid;
    }
    public void DifficultyChanged(Difficulty.Level difficulty)
    {
        waitTime = ParseDifficulty(difficulty);
    }
    float ParseDifficulty(Difficulty.Level difficulty)
    {
        switch (difficulty)
        {
            case Difficulty.Level.Easy:
                return 0.6f;
            case Difficulty.Level.Medium:
                return 0.4f;
            case Difficulty.Level.Hard:
                return 0.2f;
        }
        return 0.5f;
    }
    private IEnumerable<PathNode> CalculatePath()
    {
        if (grid == null || grid.Length == 0)
            return null;
        Player player = GameObject.Find("Player").GetComponent<Player>();
        if (PosX == player.PosX && PosY == player.PosY)
            return null;
        if (path != null)
            return path;
        EnemyAStar.MySolver<PathNode, object> aStar = new EnemyAStar.MySolver<PathNode, object>(grid);
        path = aStar.Search(new Vector2(PosX, PosY), new Vector2(player.PosX, player.PosY), null);
        return path;
    }
    public IEnumerator GoNow()
    {
        while (true)
        {
            if (MoveAllowed)
            {
                var path = CalculatePath();
                if (path != null)
                {
                    foreach (PathNode x in path)
                    {
                        if (!x.IsWall)
                        {
                            PosX = x.X;
                            PosY = x.Y;
                            yield return new WaitForSeconds(waitTime);
                        }
                    }
                }
            }
            yield return null;
        }
    }
    public void InvalidatePath()
    {
        path = null;
    }
    public void StartMoving()
    {
        StartCoroutine(GoNow());
        MoveAllowed = true;
        Visible = true;
    }
    public bool Visible
    {
        get
        {
            return gameObject.GetComponent<Renderer>().enabled;
        }
        set
        {
            Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
            foreach (Renderer r in renderers)
            {
                r.enabled = value;
            }
        }
    }
}

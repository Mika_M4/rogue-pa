﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public static GameController Instance;
    private BoardController boardController;
    private GameObject UI, gameOverScreen, startScreen;
    private Transform tr;
    private Vector3 pos;
    private Text _levelText;
    private bool FadeInLevelText, FadeOutLevelText;
    void Awake()
    {
        UIScreen = true;
        GameOverScreen = false;
        var dataHolder = GetDataHolder();
        int level = dataHolder.CurrentLevel++;
        boardController = GetComponent<BoardController>();
        boardController.SetupLevel(level);
        GameObject.Find("Enemy").GetComponent<Enemy>().DifficultyChanged(GetDataHolder().Difficulty);
    }

    void Start()
    {
        Player player = GameObject.Find("Player").GetComponent<Player>();
        pos = player.transform.position;
        tr = player.transform;
    }

    void Update()
    {
        if (UIScreen)
            return;
        if (FadeOutLevelText)
        {
            LevelText.color = Color.Lerp(LevelText.color, Color.clear, 3 * Time.deltaTime);
            if (LevelText.color == Color.clear)
                FadeOutLevelText = false;
        }
        else if (FadeInLevelText)
        {
            LevelText.color = Color.Lerp(LevelText.color, Color.white, 3 * Time.deltaTime);
            if (LevelText.color == Color.white)
            {
                FadeInLevelText = false;
                FadeOutLevelText = true;
            }
        }
        Player player = GameObject.Find("Player").GetComponent<Player>();
        Vector3 posClone = pos;
        bool move = false;
        if (Input.GetKeyDown(KeyCode.RightArrow) && tr.position == pos && player.MoveAllowed)
        {
            player.Flip(MovingObject.Rotation.Right);
            pos += Vector3.right;
            move = true;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) && tr.position == pos && player.MoveAllowed)
        {
            player.Flip(MovingObject.Rotation.Left);
            pos += Vector3.left;
            move = true;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) && tr.position == pos && player.MoveAllowed)
        {
            player.Flip(MovingObject.Rotation.Up);
            pos += Vector3.up;
            move = true;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) && tr.position == pos && player.MoveAllowed)
        {
            player.Flip(MovingObject.Rotation.Down);
            pos += Vector3.down;
            move = true;
        }
        Vector2 endPosition = new Vector2(pos.x, pos.y);
        Vector2 startPosition = new Vector2(player.transform.position.x, player.transform.position.y);
        if (player.CanMove(startPosition, endPosition))
        {
            player.Move(endPosition);
            GameObject.Find("Enemy").GetComponent<Enemy>().InvalidatePath();
        }
        else
            pos = posClone;
        
    }
    private void OnLevelWasLoaded(int level)
    {
        UIScreen = false;
        GameObject.Find("Enemy").GetComponent<Enemy>().DifficultyChanged(GetDataHolder().Difficulty);
        PopupLevelText();
    }
    public void NextLevel()
    {
        Enemy enemy = GameObject.Find("Enemy").GetComponent<Enemy>();
        enemy.StopAllCoroutines();
        enemy.MoveAllowed = false;
        Invoke("NewGame", 2);
    }

    public void NewGame()
    {
        var audio = GetDataHolder().GetAudioSource();
        if (!audio.isPlaying)
            audio.Play();
        Application.LoadLevel(Application.loadedLevel);
    }
    public void StartGame()
    {
        var audio = GetDataHolder().GetAudioSource();
        audio.Play();
        UIScreen = false;
        PopupLevelText();
    }
    public void GameOver()
    {
        var audio = GetDataHolder().GetAudioSource();
        audio.Stop();
        GetDataHolder().CurrentLevel = 0;
        UIScreen = true;
        StartScreen = false;
        GameOverScreen = true;
    }
    internal GameDataHolder GetDataHolder()
    {
        return GameObject.Find("DataHolder").GetComponent<GameDataHolder>();
    }
    public void OnDifficultyChanged()
    {
        Slider slider = GameObject.Find("Difficulty Slider").GetComponent<Slider>();
        Difficulty.Level difficulty = (Difficulty.Level)slider.value;
        GameObject.Find("Difficulty Text").GetComponent<Text>().text = "Difficulty: " + difficulty.ToString();
        GetDataHolder().Difficulty = difficulty;
        GameObject.Find("Enemy").GetComponent<Enemy>().DifficultyChanged(difficulty);
    }
    public void PopupLevelText()
    {
        LevelText.text = string.Format("Level {0} ({1}x{2})", GetDataHolder().CurrentLevel, boardController.columns, boardController.rows);
        FadeInLevelText = true;
    }
    public bool UIScreen
    {
        get
        {
            if (UI == null)
                UI = GameObject.Find("UI");
            return UI.activeSelf;
        }
        set
        {
            if (UI == null) 
                UI = GameObject.Find("UI"); 
            UI.SetActive(value);
        }
    }
    public bool StartScreen
    {
        get
        {
            if (startScreen == null)
                startScreen = GameObject.Find("StartScreen");
            return startScreen.activeSelf;
        }
        set
        {
            if (startScreen == null)
                startScreen = GameObject.Find("StartScreen");
            startScreen.SetActive(value);
        }
    }
    public bool GameOverScreen
    {

        get
        {
            if (gameOverScreen == null)
                gameOverScreen = GameObject.Find("GameOver Screen");
            return gameOverScreen.activeSelf;
        }
        set
        {
            if (gameOverScreen == null)
                gameOverScreen = GameObject.Find("GameOver Screen");
            gameOverScreen.SetActive(value);
        }
    }
    public Text LevelText
    {
        get
        {
            if (_levelText == null) 
                _levelText =  GameObject.Find("LevelText").GetComponent<Text>();
            return _levelText;
        }
    }

}

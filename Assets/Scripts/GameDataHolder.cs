﻿using UnityEngine;
public class GameDataHolder : MonoBehaviour {
    internal int CurrentLevel = 0;
    internal Difficulty.Level Difficulty = global::Difficulty.Level.Easy;
	void Start() {
        DontDestroyOnLoad(this);
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }
    internal AudioSource GetAudioSource()
    {
        return GameObject.Find("SoundController").GetComponent<AudioSource>();
    }
}

﻿using UnityEngine;
using System;
public class MovingObject : MonoBehaviour
{
    private BoxCollider2D boxCollider;
    private SpriteRenderer spriteRenderer;
    
    public Sprite RightSided;
    public Sprite LeftSided;
    public Sprite UpSided;
    public Sprite DownSided;
    public Sprite OnFinish;

    public float OffsetX;
    public float OffsetY;
    public Rounding RoundX;
    public Rounding RoundY;

    internal bool MoveAllowed;
    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        //animator = GetComponent<Animation>();
    }
    public bool CanMove(Vector2 startPosition, Vector2 endPosition)
    {
        if (startPosition == endPosition)
            return false;
        if (boxCollider == null)
        {
            boxCollider = GetComponent<BoxCollider2D>();
        }
        boxCollider.enabled = false;
        RaycastHit2D hit = Physics2D.Linecast(startPosition, endPosition);
        boxCollider.enabled = true;
        
        return hit.transform == null;
    }
    public void Flip(Rotation rotation)
    {
        if (spriteRenderer == null)
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
        Sprite sprite = null;
        switch (rotation)
        {
            case Rotation.Left:
                sprite = LeftSided;
                break;
            case Rotation.Right:
                sprite = RightSided;
                break;
            case Rotation.Up:
                sprite = UpSided;
                break;
            case Rotation.Down:
                sprite = DownSided;
                break;
        }
        spriteRenderer.sprite = sprite;
    }
    public void Move(Vector3 position, float speed = 3f)
    {
        if (this is Player)
        {
            if (PosX > 3 || PosY > 3)
            {
                Enemy enemy = GameObject.Find("Enemy").GetComponent<Enemy>();
                if (!enemy.Visible) enemy.StartMoving();
            }
        }
        transform.position = Vector3.MoveTowards(transform.position, position, Time.deltaTime * speed);
    }
    public enum Rotation
    {
        Left,
        Right,
        Up,
        Down
    }
    public int PosX
    {
        get
        {
            switch (RoundX)
            {
                case Rounding.Up:
                    return Mathf.CeilToInt(transform.position.x);
                case Rounding.Down:
                    return Mathf.FloorToInt(transform.position.x);
            }
            return 0;
        }
        set
        {
            if (value < PosX)
            {
                Flip(Rotation.Left);
            }
            else if(value > PosX)
            {
                Flip(Rotation.Right);
            }
            switch(RoundX)
            {
                case Rounding.Up:
                    transform.position = new Vector2(OffsetX - 1 + value, transform.position.y);
                    break;
                case Rounding.Down:
                    transform.position = new Vector2(OffsetX + value, transform.position.y);
                    break;
            }
        }
    }
    public int PosY
    {
        get
        {
            switch (RoundY)
            {
                case Rounding.Up:
                    return Mathf.CeilToInt(transform.position.y);
                case Rounding.Down:
                    return Mathf.FloorToInt(transform.position.y);
            }
            return 0;
        }
        set
        {
            if (value > PosY)
            {
                Flip(Rotation.Up);
            }
            else if (value < PosY)
            {
                Flip(Rotation.Down);
            }
            switch (RoundY)
            {
                case Rounding.Up:
                    transform.position = new Vector2(transform.position.x, value - OffsetY);
                    break;
                case Rounding.Down:
                    transform.position = new Vector2(transform.position.x, value + (OffsetY - 1));
                    break;
            }
        }
    }
    public enum Rounding
    {
        Up,
        Down
    }
}

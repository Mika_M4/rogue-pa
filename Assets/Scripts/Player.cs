﻿using UnityEngine;

public class Player : MovingObject
{
    public void Start()
    {
        MoveAllowed = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Enemy")
        {
            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            if (enemy.Visible && enemy.PosX == PosX && enemy.PosY == PosY)
            {
                enemy.StopAllCoroutines();
                enemy.MoveAllowed = false;
                GameObject.Find("GameController").GetComponent<GameController>().GameOver();
            } 
        }
        else if (collision.tag == "Finish")
        {
            MoveAllowed = false;
            GetComponent<SpriteRenderer>().sprite = OnFinish;
            GameObject.Find("GameController").GetComponent<GameController>().NextLevel();
        }
    }
}

﻿using UnityEngine;
public class SoundController : MonoBehaviour {
    void Start () {
        DontDestroyOnLoad(this);
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }
}
